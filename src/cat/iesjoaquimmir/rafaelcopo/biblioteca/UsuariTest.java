package cat.iesjoaquimmir.rafaelcopo.biblioteca;

import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.Categoria;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.multimedia.DiscCompacte;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.multimedia.DiscVersatilDigital;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.publicacio.Llibre;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.publicacio.Revista;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.usuaris.AbstractUsuari;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.usuaris.TipusUsuari;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.usuaris.UsuariAdult;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.usuaris.UsuariMenorEdat;

import java.util.ArrayList;
//import java.util.Scanner;

public class UsuariTest {
    public static void main(String[] args) {

        // el segon paràmetre de l'usuari adult indica si es premium
        UsuariAdult usuariPremium = new UsuariAdult("498878974K", TipusUsuari.P, "Pepe", "Carrasco", "Coronado");
        UsuariAdult usuariNormal = new UsuariAdult("498878214M", TipusUsuari.N, "Paco", "Soler", "Picasso");
        UsuariMenorEdat usuariMenorPremium = new UsuariMenorEdat(usuariPremium, "Pedro", "Garcia", "Saez");
        UsuariMenorEdat usuariMenorNormal = new UsuariMenorEdat(usuariNormal, "Paulo", "Lopez", "Moliere");
        
        DiscCompacte a0 = new DiscCompacte("Mis grandes éxitos", "Luis Miguel", Categoria.A, 65, "04900009");
	DiscVersatilDigital a1 = new DiscVersatilDigital("Bamby", "Walt Disney", Categoria.I, 89, "90843089");
	DiscVersatilDigital a2 = new DiscVersatilDigital("Crepusculo", "Warner", Categoria.J, 129, "480980983");
	Llibre a3 = new Llibre("Ulises", "James Joyce", Categoria.J, 456, "940943322");
	Llibre a4 = new Llibre("Tina Super Bruixa", "Enid Blyton", Categoria.J, 460, "342090233");
	Llibre a7 = new Llibre("Tina Super Bruixa", "Enid Blyton", Categoria.J, 460, "342090234");

        Revista a5 = new Revista("Patufet", "Josep M. Folch i Torres", Categoria.I, 87, "80002122");
    
        ArrayList<AbstractUsuari> usuaris = new ArrayList<>();
	usuaris.add(usuariPremium);
        usuaris.add(usuariNormal);
        usuaris.add(usuariMenorPremium);
        usuaris.add(usuariMenorNormal);
        
        usuariPremium.agafaArticle(a0);
        usuariPremium.agafaArticle(a1);
        usuariPremium.retornaArticle(a0);
        usuariNormal.agafaArticle(a5);
        usuariMenorNormal.agafaArticle(a1);
        usuariNormal.agafaArticle(a0);
        usuariMenorPremium.agafaArticle(a3);
        usuariMenorPremium.agafaArticle(a4);
        usuariMenorPremium.agafaArticle(a7);
        
	for(int i=0;i<usuaris.size();++i) {
            System.out.println(usuaris.get(i).toString());
            System.out.printf("Articles Usuari: %d%n%n",usuaris.get(i).getSizeArticlesUsuari());
            for(int j=0;j<usuaris.get(i).getSizeArticlesUsuari();++j) {
                System.out.println(usuaris.get(i).getArticle(j).toString());
            }
	}
        
        System.gc();
        System.exit(0);
   }

}
