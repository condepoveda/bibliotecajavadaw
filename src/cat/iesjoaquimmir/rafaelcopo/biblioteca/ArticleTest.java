package cat.iesjoaquimmir.rafaelcopo.biblioteca;

import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.Categoria;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.publicacio.Llibre;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.publicacio.Revista;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.multimedia.DiscCompacte;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.multimedia.DiscVersatilDigital;

//import java.util.ArrayList;

public class ArticleTest {

    public static void main(String[] args) {
        // TODO code application logic here
        // L'últim paràmetre és el codi isbn, issn, ismn, isan segons sigui llibre, revista, cd o dvd respectivament
        DiscCompacte a0 = new DiscCompacte("Mis grandes éxitos", "Luis Miguel", Categoria.A, 65, "04900009");
        DiscVersatilDigital a1 = new DiscVersatilDigital("Bamby", "Walt Disney", Categoria.I, 89, "90843089");
        DiscVersatilDigital a2 = new DiscVersatilDigital("Crepusculo", "Warner", Categoria.J, 129, "480980983");
        Llibre a3 = new Llibre("Ulises", "James Joyce", Categoria.A, 456, "940943322");
        Llibre a4 = new Llibre("Tina Super Bruixa", "Enid Blyton", Categoria.J, 456, "342090233");
        Revista a5 = new Revista("Patufet", "Ed. Infantil", Categoria.I, 87, "80002122");
        
        //impresion
        System.out.printf("%s", a0.toString());
        System.out.printf("%s", a1.toString());
        System.out.printf("%s", a2.toString());
        System.out.printf("%s", a3.toString());
        System.out.printf("%s", a4.toString());
        System.out.printf("%s", a5.toString());
        
        
        System.gc();
        System.exit(0);   
    }
}
