/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.rafaelcopo.biblioteca.usuaris;

import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.AbstractArticle;

/**
 *
 * @author tarda
 */
public interface InUsuari {
    
    public static final int ARTICLES_NORMAL = 2;
    public static final int ARTICLES_PREMIUM = 4;

    public String getNom();
    public String getCognom1();
    public String getCognom2();
    
    public boolean potAgafarArticle(AbstractArticle a);

    public void agafaArticle(AbstractArticle a);
    public boolean teArticle(AbstractArticle a);
    public void retornaArticle(AbstractArticle a);
    
    public AbstractArticle getArticle(int index);
    public int getSizeArticlesUsuari();
    
}
