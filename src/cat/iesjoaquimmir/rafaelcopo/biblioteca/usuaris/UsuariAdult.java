/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.rafaelcopo.biblioteca.usuaris;

/**
 *
 * @author tarda
 */
public class UsuariAdult extends AbstractUsuari{
//<editor-fold defaultstate="collapsed" desc="ATRIBUTS">
    private String dni;
    private TipusUsuari premium;
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="MÈTODES">
    //<editor-fold defaultstate="collapsed" desc="getters&setters">
    /**
     * @return the dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * @param dni the dni to set
     */
    private void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * @return the premium
     */
    protected TipusUsuari getPremium() {
        return premium;
    }

    /**
     * @param premium the premium to set
     */
    private void setPremium(TipusUsuari premium) {
        this.premium = premium;
    }    
    //</editor-fold>    

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public UsuariAdult(String dni, TipusUsuari premium, String nom, String cognom1, String cognom2) {
        super(nom, cognom1, cognom2);
        this.setDni(dni);
        this.setPremium(premium);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Sobrescritura">
    /**
     *
     * @return
     */
    @Override
    protected boolean isPremium() {
        return getPremium().getPremium();
    }

    @Override
    protected boolean isMajorEdat() {
        return true;
    }
    @Override
    public String toString() {
        return String.format("Usuari Adult:%n------------%nDni: %s %n"
                + "%s"
                + "Tipus usuari: %s %n", this.getDni(), super.toString(), this.getPremium().getDescripcio());
    }
    //</editor-fold>
//</editor-fold>   
}
