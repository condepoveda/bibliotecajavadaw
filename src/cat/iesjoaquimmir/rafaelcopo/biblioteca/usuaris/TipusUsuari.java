/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.rafaelcopo.biblioteca.usuaris;

/**
 *
 * @author tarda
 */
public enum TipusUsuari {
//<editor-fold defaultstate="collapsed" desc="Estat:ATRIBUTS">

    /**
     *
     */
    P("Premium", true, 4),
    N("Normal", false, 2);
    private String descripcio;
    private boolean premium;
    private int numArticlesMax;
//<editor-fold defaultstate="collapsed" desc="MÈTODES">
    //<editor-fold defaultstate="collapsed" desc="getters&setters">
    /**
     * @return the descripcio
     */
    public String getDescripcio() {
        return descripcio;
    }

    /**
     * @param descripcio the descripcio to set
     */
    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    /**
     * @return the premium
     */
    public boolean getPremium() {
        return premium;
    }
    /**
     * @param premium the premium to set
     */
    public void setPremium(boolean premium) {
        this.premium = premium;
    }

    /**
     * @return the numArticlesMax
     */
    public int getNumArticlesMax() {
        return numArticlesMax;
    }

    /**
     * @param numArticlesMax the numArticlesMax to set
     */
    public void setNumArticlesMax(int numArticlesMax) {
        this.numArticlesMax = numArticlesMax;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Constructores">
    TipusUsuari(String descripcio, boolean premium, int numArticlesMax){
        this.setDescripcio(descripcio);
        this.setPremium(premium);
        this.setNumArticlesMax(numArticlesMax);
    }
    //</editor-fold>
//</editor-fold>
}
