/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.rafaelcopo.biblioteca.usuaris;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.AbstractArticle;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.Categoria;
import java.util.ArrayList;
/**
 *
 * @author tarda
 */
public abstract class AbstractUsuari implements InUsuari{
//<editor-fold defaultstate="collapsed" desc="ATRIBUTS">
    private String nom;
    private String cognom1;
    private String cognom2;
    private ArrayList<AbstractArticle> articles;    
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="MÈTODES">
    //<editor-fold defaultstate="collapsed" desc="getters&setters">
         /**
         * @return the nom
         */
        public String getNom() {
            return nom;
        }

        /**
         * @param nom the nom to set
         */
        public void setNom(String nom) {
            this.nom = nom;
        }

        /**
         * @return the cognom1
         */
        public String getCognom1() {
            return cognom1;
        }

        /**
         * @param cognom1 the cognom1 to set
         */
        public void setCognom1(String cognom1) {
            this.cognom1 = cognom1;
        }

        /**
         * @return the cognom2
         */
        public String getCognom2() {
            return cognom2;
        }

        /**
         * @param cognom2 the cognom2 to set
         */
        public void setCognom2(String cognom2) {
            this.cognom2 = cognom2;
        }
            /**
     * @return the articles
     */
    private ArrayList<AbstractArticle> getArticles() {
        return articles;
    }

    /**
     * @param articles the articles to set
     */
    public void setArticles(ArrayList<AbstractArticle> articles) {
        this.articles = articles;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="constructors">
    private AbstractUsuari(String nom, String cognom1, String cognom2, ArrayList<AbstractArticle>
    articles) {
        this.setNom(nom);
        this.setCognom1(cognom1);
        this.setCognom2(cognom2);
        this.setArticles(articles);
    }
    public AbstractUsuari(String nom, String cognom1, String cognom2) {
        this(nom,cognom1,cognom2, new ArrayList<AbstractArticle>() );
    }   
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="mètodes d'objecte">
    @Override
    public boolean potAgafarArticle(AbstractArticle a) {
	boolean pot = false;
        if (!teArticle(a)){
            if(this.getArticles().size() < this.getPremium().getNumArticlesMax()){
                if(isMajorEdat() || !a.getCategoria().equals(Categoria.A)) {
                    pot = true;
                }else{
                    System.err.printf("L'article %s és per adults i no el pot agafar l'usuari %s.\n", a.getTitol(), this.getNom());
                }
	    }else{
                System.err.printf("L'article %s ja supera el número"
                    + " màxim d'articles que pot agafar l'usuari %s.\n", a.getTitol(), this.getNom());
            }
        }
        else{
                System.err.printf("L'article %s ja el te l'usuari %s.\n", a.getTitol(), this.getNom());
        }
	return pot;
    }
    @Override
    public boolean teArticle(AbstractArticle a){
        return getArticles().contains(a);
    }
    
    @Override
    public void agafaArticle(AbstractArticle a){
        if (potAgafarArticle(a)){
            getArticles().add(a);
        }
    }
    
    @Override
    public void retornaArticle(AbstractArticle a){
        if (teArticle(a))
            getArticles().remove(a);
    }
    
    @Override
    public AbstractArticle getArticle(int index){
        return getArticles().get(index);        
    }
    
    @Override
    public int getSizeArticlesUsuari(){
        return getArticles().size();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Mètodes abastractes">
    protected abstract boolean isPremium();
    protected abstract boolean isMajorEdat();
    protected abstract TipusUsuari getPremium();
    //</editor-fold>
//</editor-fold>
}
