/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.rafaelcopo.biblioteca.usuaris;

/**
 *
 * @author tarda
 */
public final class UsuariMenorEdat extends AbstractUsuari{
//<editor-fold defaultstate="collapsed" desc="ATRIBUTS">
    private UsuariAdult tutor;
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="MÈTODES">
    //<editor-fold defaultstate="collapsed" desc="getters&setters">
     /**
     * @return the tutor
     */
    public UsuariAdult getTutor() {
        return tutor;
    }

    /**
     * @param tutor the tutor to set
     */
    private void setTutor(UsuariAdult tutor) {
        this.tutor = tutor;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="constructors">

    /**
     *
     * @param tutor
     * @param nom
     * @param cognom1
     * @param cognom2
     */
    public UsuariMenorEdat(UsuariAdult tutor,String nom, String cognom1, String cognom2) {
        super(nom, cognom1, cognom2);
        this.setTutor(tutor);
    }
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="Sobreescritura">
    @Override
    protected boolean isPremium() {
        return tutor.isPremium();
    }

    @Override
    protected boolean isMajorEdat() {
        return false;
    }
    
    @Override
    protected TipusUsuari getPremium() {
        return tutor.getPremium(); 
    }
    @Override
    public String toString() {
    return String.format("Usuari Menor:%n------------%n"
                + "%s"
                + "Tipus usuari: %s %nNom del Tutor: %s %n",  
                super.toString(), this.getPremium().getDescripcio(), this.tutor.getNom());
    }
    //</editor-fold> 
//</editor-fold>  
}
