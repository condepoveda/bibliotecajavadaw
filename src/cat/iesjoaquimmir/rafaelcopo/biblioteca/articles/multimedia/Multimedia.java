/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.multimedia;

import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.AbstractArticle;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.Categoria;

public abstract class Multimedia extends AbstractArticle{

//<editor-fold defaultstate="collapsed" desc="ATRIBUTS">
    //está en private para que sea NO modificable
    private int min;
    //private ArrayList<DiscCompacte> cd;  NO CAL PERQUE NO GUARDAREM RES
    //private ArrayList<DiscVersatilDigital> dvd;  NO CAL PERQUE NO GUARDAREM RES
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="MÈTODES">
    //<editor-fold defaultstate="collapsed" desc="constructors">
    public Multimedia(String titol, String autor, Categoria categoria, int min) {
        super(titol, autor, categoria);
        this.setMin(min);
    }      
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="setter&getters">
    public int getMin() {
        return min;
    }
    public void setMin(int min) {
        this.min = min;
    }   
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="sobreescritura">
    @Override
    public String toString(){
        return String.format("%sMin: %d,%n", 
            super.toString(),
            this.getMin());
    }
    //</editor-fold>
//</editor-fold>

}
