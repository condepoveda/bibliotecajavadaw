package cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.multimedia;

import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.Categoria;

public final class DiscVersatilDigital extends Multimedia{
//<editor-fold defaultstate="collapsed" desc="ATRIBUTS">
    private String isan;
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="MÈTODES">
    //<editor-fold defaultstate="collapsed" desc="gettes&settes">
        public String getIsan() {
            return isan;
        }
        public void setIsan(String isan) {
            this.isan = isan;
        }
    //</editor-fold>
//<editor-fold defaultstate="collapsed" desc="constructors">
    public DiscVersatilDigital(String titol, String autor, Categoria categoria, int min, String isan) {
        super(titol, autor, categoria, min);
        this.setIsan(isan);
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="sobreescritura">
    @Override
    public String toString(){
        return String.format("%sisan: %s;%n", 
            super.toString(),
            this.getIsan());
    }
    //</editor-fold>
//</editor-fold>
}