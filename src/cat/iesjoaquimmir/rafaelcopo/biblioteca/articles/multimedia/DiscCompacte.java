package cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.multimedia;

import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.Categoria;

public final class DiscCompacte extends Multimedia{
//<editor-fold defaultstate="collapsed" desc="ATRIBUTS"
    private String ismn;
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="MÈTODES">
    //<editor-fold defaultstate="collapsed" desc="getters&setters">
        public String getIsmn() {
            return ismn;
        }
        public void setIsmn(String ismn) {
            this.ismn = ismn;
        }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="constructors">       
    public DiscCompacte(String titol, String autor, Categoria categoria, int min, String ismn) {
        super(titol, autor, categoria, min);
        this.setIsmn(ismn);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="sobreescritura">
    @Override
    public String toString(){
        return String.format("%sismn: %s;%n", 
            super.toString(),
            this.getIsmn());
    }
    //</editor-fold>
//</editor-fold>
}