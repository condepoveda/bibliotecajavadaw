/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.rafaelcopo.biblioteca.articles;

public enum Categoria {
//<editor-fold defaultstate="collapsed" desc="ATRIBUTS">   
    I("INFANTIL",true), 
    J("JUVENIL",true),
    A("ADULT",true);
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="MÈTODES">
    //<editor-fold defaultstate="collapsed" desc="atributs">
    private String tipus;
    private boolean aconsellat;    
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="comment">
    Categoria(String tipus, boolean aconsellat) {
    this.tipus = tipus;
    this.aconsellat = aconsellat;
    }      
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getter&setters">
    public String getTipus() {
        return tipus;
    }
    public void setTipus(String tipus) {
        this.tipus = tipus;
    }
    public boolean isAconsellat() {
        return aconsellat;
    }
    public void setAconsellat(boolean aconsellat) {
        this.aconsellat = aconsellat;
    }
    //</editor-fold> 
//</editor-fold>
}
