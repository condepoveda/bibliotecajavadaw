/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.publicacio;

import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.AbstractArticle;
import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.Categoria;

public abstract class Publicacio extends AbstractArticle {
//<editor-fold defaultstate="collapsed" desc="ATRIBUTS">
    //está en private para que no sea modificable desde fuera de esta clase
    private int numpag;
    //private ArrayList<Llibre> llibre; NO CAL PERQUE NO GUARDAREM RES
    //private ArrayList<Revista> revista; NO CAL PERQUE NO GUARDAREM RES    
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="MÈTODES">
    //<editor-fold defaultstate="collapsed" desc="getters&settes">
    public int getNumpag() {
        return numpag;
    }

    /**
     *
     * @param numpag
     */
    private void setNumpag(int numpag) {
        this.numpag = numpag;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="constructors">
    public Publicacio(String titol, String autor, Categoria categoria, int numpag) {
        super(titol, autor, categoria);
        this.setNumpag(numpag);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="sobreescritura">
    @Override
    public String toString(){
        return String.format("%sNum de pàgines: %d,%n", 
            super.toString(),
            this.getNumpag());
    }
    //</editor-fold>
//</editor-fold>
}
