/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.publicacio;

import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.Categoria;

public final class Llibre extends Publicacio{
//<editor-fold defaultstate="collapsed" desc="ATRIBUTS">
    //private para ser información NO modificable
    private String isbn;
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="MÊTODES">
    //<editor-fold defaultstate="collapsed" desc="getters&setters">
        public String getIsbn() {
            return isbn;
        }
        public void setIsbn(String isbn) {
            this.isbn = isbn;
        }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="constructors">
        public Llibre(String titol, String autor, Categoria categoria, int numpag, String isbn) {
            super(titol, autor, categoria, numpag);
            this.setIsbn(isbn);
        }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="sobreescritura">
    @Override
    public String toString(){
        return String.format("%sisbn: %s;%n", 
            super.toString(),
            this.getIsbn());
    }
    //</editor-fold>
//</editor-fold>
}