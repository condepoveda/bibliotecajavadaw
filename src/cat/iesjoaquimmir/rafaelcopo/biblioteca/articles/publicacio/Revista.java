/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.publicacio;

import cat.iesjoaquimmir.rafaelcopo.biblioteca.articles.Categoria;

public final class Revista extends Publicacio{

//<editor-fold defaultstate="collapsed" desc="ATRIBUTS">
    private String issn;   
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="MÈTODES">
    //<editor-fold defaultstate="collapsed" desc="getters&setters">
    public String getIssn() {
        return issn;
    }
    public void setIssn(String issn) {
        this.issn = issn;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="constructors">
        public Revista(String titol, String autor, Categoria categoria, int numpag, String issn) {
            super(titol, autor, categoria, numpag);
            this.setIssn(issn);
        }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="sobreescritura">
    @Override
    public String toString(){
        return String.format("%sissn: %s;%n", 
            super.toString(),
            this.getIssn());
    }
    //</editor-fold>
//</editor-fold>
}