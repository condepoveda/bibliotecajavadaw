/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.rafaelcopo.biblioteca.articles;

public abstract class AbstractArticle implements InArticle{
//<editor-fold defaultstate="collapsed" desc="ATRIBUTS">
    //Al ser private es NO modificable desde fuera de esta clase.
    private String titol;
    private String autor;
    private Categoria categoria;
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="METODES">
    //<editor-fold defaultstate="collapsed" desc="getters&setters">
    public String getTitol() {
        return titol;
    }
    private void setTitol(String titol) {
        //lo ponemos private para que no accedan desde fuera
        if(titol == null || "".equals(titol)){
            throw new IllegalArgumentException();
        }
        this.titol = titol;
    }
    public String getAutor() {
        return autor;
    }
    public void setAutor(String autor) {
        this.autor = autor;
    }
    public Categoria getCategoria() {
        return categoria;
    }
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Constructors">
    public AbstractArticle(String titol, String autor, Categoria categoria){
        this.setTitol(titol);
        this.setAutor(autor);
        this.setCategoria(categoria);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="sobreescritura">
    @Override
    public String toString(){
        return String.format("%n----------%nDVD: %s,%n"
            + "Autor: %s,%n"
            + "Categoria: %s,%n", 
            this.getTitol(),
            this.getAutor(),
            this.getCategoria().getTipus());
    }
    //</editor-fold>
//</editor-fold>   
}
